const modal = document.getElementById('modal');

const openModal = message => {
  document.getElementById('modal-message').innerHTML = message;
  modal.classList.add('open');
}

const confirm = (onCloseCallback) => {
  modal.classList.remove('open');
  onCloseCallback(true)
}

const cancel = (onCloseCallback) => {
  modal.classList.remove('open');
  onCloseCallback(false)
}
